#include<iostream>
#include<iomanip>
#include<cstdlib>
#include<string>
#include<ctime>

using namespace std;

void stepback(string *a);

int main(){

	int space[13]={1,2,3,4,5,6,7,8,9,10,11,12,13};
	string card[13]={"K","Q","J","10","9","8","7","6","5","4","3","2","A"};
	string rdm[13]={""};

	string counter=card[12];
	int count=0;

	srand(time(NULL));

	for(int i=0;i<13;i++){//put A-K in another array in random order
		int r=rand()%13;

		if(space[r]!=0){
			rdm[r]=card[i];
			space[r]=0;
		}
		else i--;
		
	}

	while(rdm[0]!=""){//repeat until array is empty

		for(int i=0;i<13;i++){
			if(rdm[i]!="")cout<<rdm[i];
		}
		cout<<endl;

		while(rdm[0]!=card[count]){//move until the largest card at first place of array
			
			stepback(rdm);

			for(int i=0;i<13;i++){
				if(rdm[i]!="")cout<<rdm[i];
			}
			cout<<endl;
		}

		rdm[0]="";//draw out the largest card
		stepback(rdm);

		count++;
	}

	return 0;
}

void stepback(string *a){

	string rgter=a[0];

	for(int i=1;i<13;i++){
		if(a[i]!=""){
			a[i-1]=a[i];
			if(i==12)a[i]=rgter;
		}
		else {
			a[i-1]=rgter;
			break;
		}
	}
}
